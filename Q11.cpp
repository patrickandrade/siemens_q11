/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <bits/stdc++.h>
using namespace std;
int main()
{
   string s;
   // Read a entire string (including spaces)
   getline (cin, s);
   int n=s.length();
   int start=0,end=n-1;
   // While is not end of string
   while(start<end)
   {
		// XOR between the (first + current_step) and the (last - current_step) characters | This creates a mask
		s[start]^=s[end];
		// XOR between the (end + current_step) and the (fist - current_step) characters | This put the (fist - current_step) on the (end + current_step)
		s[end]^=s[start];
		// XOR between the (first + current_step) and the (last - current_step) characters | This put the (end + current_step) on the (fist - current_step)
		s[start]^=s[end];
		// Increment of step
		start++;
		end--;
   }
   cout<<s<<endl;
   return 0;
}