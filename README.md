## Question 11 for Siemens Programming skills and PL/SQL

This repository holds code for the eleventh question of Siemens Programming skills and PL/SQL.

The code consists of a program that reverses a string informed by the user without using any temporary variable,
buffer or any pre-existing function or method for this.
